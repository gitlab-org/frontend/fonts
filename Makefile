INTER_VERSION = fd99812ed05e9bc775ed8a71667f9214e72ba925
JETBRAINS_MONO_VERSION = cd5227bd1f61dff3bbd6c814ceaf7ffd95e947d9

.PHONY: clean

all: jetbrains-mono gitlab-sans gitlab-mono pages

clean:
	rm -rf .tmp .venv jetbrains-mono gitlab-sans gitlab-mono pages/fonts pages/index.html sources/gitlab-mono/*.glyphs sources/gitlab-mono/master_ufo

## Jetbrains Mono

jetbrains-mono: jetbrains-mono/OFL.txt jetbrains-mono/JetBrainsMono.woff2 jetbrains-mono/JetBrainsMono-Bold.woff2 jetbrains-mono/JetBrainsMono-Italic.woff2 jetbrains-mono/JetBrainsMono-BoldItalic.woff2

jetbrains-mono/OFL.txt: .tmp/jetbrains-mono
	mkdir -p jetbrains-mono
	cp .tmp/jetbrains-mono/OFL.txt $@

jetbrains-mono/JetBrainsMono.woff2: .tmp/jetbrains-mono
	mkdir -p jetbrains-mono
	cp .tmp/jetbrains-mono/fonts/webfonts/JetBrainsMono-Regular.woff2 $@

jetbrains-mono/%.woff2: .tmp/jetbrains-mono
	mkdir -p jetbrains-mono
	cp .tmp/jetbrains-mono/fonts/webfonts/$(notdir $@) $@

.tmp/jetbrains-mono/fonts/variable/%.ttf: .tmp/jetbrains-mono

.tmp/jetbrains-mono: .tmp/jm-${JETBRAINS_MONO_VERSION}.zip
	rm -rf $@
	unzip -q -d ".tmp" $<
	mv ".tmp/JetBrainsMono-$(JETBRAINS_MONO_VERSION)" $@
	touch $@

.tmp/jm-${JETBRAINS_MONO_VERSION}.zip:
	mkdir -p $(@D)
	curl --location  --fail --output $@ "https://github.com/JetBrains/JetBrainsMono/archive/$(JETBRAINS_MONO_VERSION).zip"
	touch $@

## GitLab Mono

gitlab-mono: gitlab-mono/GitLabMono.woff2 gitlab-mono/GitLabMono-Italic.woff2 gitlab-mono/GitLabMono.ttf gitlab-mono/GitLabMono-Italic.ttf gitlab-mono/OFL.txt

### Copying license file.
###
### Has a dependency on GitLabMono.woff2 because gftools deletes the complete folder
gitlab-mono/OFL.txt: .tmp/jetbrains-mono
	mkdir -p $(@D)
	awk 'NR==2 {print "Portions Copyright 2022 Gitlab B.V."} 1' .tmp/jetbrains-mono/OFL.txt > $@
	head $@

### Building GitLab Mono ttf.
###
### Only builds the regular variable-width version.
### 1. Removes ligatures with pyftsubset https://fonttools.readthedocs.io/en/latest/subset/index.html
### 2. Updates Metadata (Copyright notice and everything)
gitlab-mono/GitLab%.ttf: .tmp/gitlab-mono/JetBrains%[wght].ttf .venv ./scripts/update_metadata.py
	mkdir -p $(@D)
	pdm run pyftsubset $< --layout-features-='calt' --unicodes="*" --name-IDs="*" --name-legacy --name-languages='*' --output-file=$@
	pdm run ./scripts/update_metadata.py --metadata GitLabMono $@

.tmp/gitlab-mono/JetBrainsMono[wght].ttf .tmp/gitlab-mono/JetBrainsMono-Italic[wght].ttf: .tmp/gitlab-mono

### Builds a patched version of JetBrains Mono and JetBrains Mono Italic
.tmp/gitlab-mono: sources/gitlab-mono/JetBrainsMono-Italic.glyphs sources/gitlab-mono/JetBrainsMono.glyphs sources/gitlab-mono/gitlab-mono.yml .venv
	pdm run gftools builder sources/gitlab-mono/gitlab-mono.yml

### Patches glyphs file
###
### Fixes https://github.com/JetBrains/JetBrainsMono/issues/567 by removing one glyph substitution
sources/gitlab-mono/%.glyphs: .tmp/jetbrains-mono sources/gitlab-mono/%.backtick.patch
	patch --unified .tmp/jetbrains-mono/sources/$(notdir $@) --output=$@ < $(basename $@).backtick.patch

## GitLab Sans

gitlab-sans: gitlab-sans/LICENSE.txt \
	gitlab-sans/GitLabSans.woff2 gitlab-sans/GitLabSans.otf gitlab-sans/GitLabSans.ttf \
	gitlab-sans/GitLabSans-Italic.woff2 gitlab-sans/GitLabSans-Italic.otf gitlab-sans/GitLabSans-Italic.ttf \

gitlab-sans/LICENSE.txt: gitlab-sans/GitLabSans.woff2
	awk 'NR==2 {print "Portions Copyright 2022 Gitlab B.V."} 1' .tmp/inter/LICENSE.txt > $@
	head $@

gitlab-sans/GitLabSans.%tf: .tmp/inter/build/fonts/var/.Inter-Roman.var.%tf .venv ./scripts/update_metadata.py
	mkdir -p gitlab-sans
	pdm run pyftfeatfreeze -f "cv05" $< $@
	pdm run ./scripts/update_metadata.py --metadata GitLabSans $@

gitlab-sans/GitLabSans-Italic.%tf: .tmp/inter/build/fonts/var/.Inter-Italic.var.%tf .venv ./scripts/update_metadata.py
	mkdir -p gitlab-sans
	pdm run pyftfeatfreeze -f "cv05" $< $@
	pdm run ./scripts/update_metadata.py --metadata GitLabSans $@

.tmp/inter: .tmp/inter-${INTER_VERSION}.zip
	rm -rf $@
	unzip -q -d ".tmp" $<
	mv .tmp/inter-${INTER_VERSION} $@
	touch $@

.tmp/inter-${INTER_VERSION}.zip:
	mkdir -p $(@D)
	curl --location  --fail --output $@ "https://github.com/rsms/inter/archive/$(INTER_VERSION).zip"

.tmp/inter/build/fonts/var/%: .tmp/inter
	$(MAKE) -C .tmp/inter/ $(@:.tmp/inter/%=%)


## Pages

pages: pages/index.html pages/fonts pages/specimen

pages/fonts: pages/fonts/Inter-Variable.woff2 pages/fonts/Inter-Variable-Italic.woff2 \
	pages/fonts/GitLabSans.otf pages/fonts/GitLabSans.ttf \
	pages/fonts/GitLabSans-Italic.otf pages/fonts/GitLabSans-Italic.ttf \
	pages/fonts/GitLabSans.woff2 pages/fonts/GitLabSans-Italic.woff2 \
	pages/fonts/GitLabMono.woff2 pages/fonts/GitLabMono-Italic.woff2 \
	pages/fonts/GitLabMono.ttf pages/fonts/GitLabMono-Italic.ttf \
	pages/fonts/JetBrainsMono.woff2 pages/fonts/JetBrainsMono-Bold.woff2 pages/fonts/JetBrainsMono-Italic.woff2 pages/fonts/JetBrainsMono-BoldItalic.woff2 \
	pages/fonts/GitLabSans.hinted.woff2 pages/fonts/GitLabSans-Italic.hinted.woff2

### Copy all fonts we build previously
pages/fonts/Inter%.woff2: .tmp/Inter%.woff2
	mkdir -p pages/fonts
	cp $< $@

pages/fonts/JetBrains%.woff2: jetbrains-mono/JetBrains%.woff2
	mkdir -p pages/fonts
	cp $< $@

pages/fonts/GitLabSans%: gitlab-sans/GitLabSans%
	mkdir -p pages/fonts
	cp $< $@

pages/fonts/GitLabMono%: gitlab-mono/GitLabMono%
	mkdir -p pages/fonts
	cp $< $@

.tmp/specimen-raw:
	git clone https://gitlab.com/gitlab-org/frontend/specimen-builder.git .tmp/specimen-raw

.tmp/specimen/%: pages/fonts/%.woff2 .tmp/specimen-raw
	mkdir -p .tmp/specimen
	$(MAKE) -C .tmp/specimen-raw/ clean
	cp $< .tmp/specimen-raw/src/fonts/
	$(MAKE) -C .tmp/specimen-raw/
	cp -r .tmp/specimen-raw/_site $@

pages/specimen/gitlab-mono: .tmp/specimen/GitLabMono
	mkdir -p pages/specimen
	cp -r $< $@

pages/specimen/gitlab-mono-italic: .tmp/specimen/GitLabMono-Italic
	mkdir -p pages/specimen
	cp -r $< $@

pages/specimen/gitlab-sans: .tmp/specimen/GitLabSans
	mkdir -p pages/specimen
	cp -r $< $@

pages/specimen/gitlab-sans-italic: .tmp/specimen/GitLabSans-Italic
	mkdir -p pages/specimen
	cp -r $< $@

## Specimen pages. We need to build them in order, one at a time.
## (therefore starting with a | character)
pages/specimen: | pages/specimen/gitlab-mono pages/specimen/gitlab-mono-italic \
	pages/specimen/gitlab-sans pages/specimen/gitlab-sans-italic

### We need Inter as well, because we want to compare GitLab Sans to it
.tmp/Inter-%.ttf: .tmp/inter/build/fonts/var/Inter-%.ttf
	cp $< $@

pages/index.html: .tmp/gitlab-sans.info.csv .tmp/gitlab-mono.info.csv pages/_index.html .venv
	pdm run ./scripts/render_pages.py pages/_index.html .tmp/gitlab-sans.info.csv .tmp/gitlab-mono.info.csv > $@

## Font info

.tmp/gitlab-sans.info.json: .tmp/fi/.tmp/Inter-Variable.ttf.json .tmp/fi/gitlab-sans/GitLabSans.ttf.json \
	.tmp/fi/.tmp/Inter-Variable-Italic.ttf.json .tmp/fi/pages/fonts/GitLabSans-Italic.ttf.json
	cat $^ | jq -s > $@

.tmp/gitlab-mono.info.json: .tmp/fi/.tmp/jetbrains-mono/fonts/variable/JetBrainsMono[wght].ttf.json .tmp/fi/gitlab-mono/GitLabMono.ttf.json \
	.tmp/fi/.tmp/jetbrains-mono/fonts/variable/JetBrainsMono-Italic[wght].ttf.json .tmp/fi/gitlab-mono/GitLabMono-Italic.ttf.json
	cat $^ | jq -s > $@

.tmp/%.info.csv: .tmp/%.info.json
	cat $< | jq '. as $$files | map(keys) | flatten | unique | .[] as $$key | [$$key] + ($$files | map(.[$$key] // "`N/A`")) | @csv' -r > $@

.tmp/fi/%.json: % .venv
	mkdir -p $(@D)
	pdm run .tmp/inter/misc/tools/fontinfo.py -pretty $< | jq '.[0].names' | jq '.["#0_file"] = "$(notdir $<)"' > $@

## General targets

%.hinted.ttf: %.ttf .venv
	pdm run python -m ttfautohint --no-info "$<" "$@"

### Conversion of ttf to woff2
%.woff2: %.ttf .venv
	mkdir -p $(@D)
	pdm run scripts/woff2.py compress -o "$@" "$<"

## Setup of venv for pyftfeatfreeze, fontutils and everything

.venv: pdm.lock pyproject.toml
	pdm install
	touch $@
