#!/usr/bin/env python
# Copy of https://github.com/rsms/inter/blob/585f56cee5c1a75711e20c55e7a2dd2ac519d6cb/misc/tools/woff2
import sys
from fontTools.ttLib.woff2 import main

if __name__ == "__main__":
  sys.exit(main())
