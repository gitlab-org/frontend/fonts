#!/usr/bin/env python3
"""Script to update metadata inside the input font's `name` table.

Currently only supports updating GitLabSans and the values need to be changed
in this file. The font file is updated in place.
"""
from argparse import ArgumentParser
from fontTools.ttLib import TTFont

# Adaptation of:
# - https://github.com/fonttools/fonttools/blob/5a0dc4bc8dfaa0c7da146cf902395f748b3cebe5/Snippets/rename-fonts.py
# - https://github.com/rsms/inter/blob/4d5d2ee06ae116fd89eab20b692fe9bf4daea9c1/misc/tools/rename.py
# - https://github.com/rsms/inter/blob/4d5d2ee06ae116fd89eab20b692fe9bf4daea9c1/misc/tools/postprocess-vf.py

WINDOWS_ENGLISH_IDS = 3, 1, 0x409
MAC_ROMAN_IDS = 1, 0, 0

LEGACY_FAMILY = 1
SUBFAMILY_NAME = 2
TRUETYPE_UNIQUE_ID = 3
FULL_NAME = 4
POSTSCRIPT_NAME = 6
PREFERRED_FAMILY = 16
TYPO_SUBFAMILY_NAME = 17
WWS_FAMILY = 21
VAR_PS_NAME_PREFIX = 25

FAMILY_RELATED_IDS = {
    LEGACY_FAMILY,
    FULL_NAME,
    POSTSCRIPT_NAME,
    PREFERRED_FAMILY,
    WWS_FAMILY,
    VAR_PS_NAME_PREFIX,
}

COPY_RIGHT_NOTICE = 0
TRADE_MARK_NOTICE = 7
MANUFACTURER_NAME = 8
VENDOR_URL = 11

# This dictionary helps to update certain metadata keys.
# You can use `{}` to retain the existing values
# More info on which metadata fields the name table has:
# https://developer.apple.com/fonts/TrueType-Reference-Manual/RM06/Chap6name.html
MetaDataUpdate = {
    COPY_RIGHT_NOTICE: "Portions Copyright 2022 Gitlab B.V. {}",
    TRADE_MARK_NOTICE: "{} GitLab is a trademark of Gitlab B.V.",
    MANUFACTURER_NAME: "Modifications by GitLab B.V., manufactured by {}",
    VENDOR_URL: "https://gitlab.com/gitlab-org/frontend/fonts"
}

MetaData = {
    # Settings for GitLab Sans.
    "GitLabSans": {
        "full_name": "GitLab Sans",
        "metadata": MetaDataUpdate
    },
    # Settings for GitLab Mono.
    "GitLabMono": {
        "full_name": "GitLab Mono",
        "metadata": MetaDataUpdate
    }
}


def set_full_name(font, full_name, full_name_ps, suffix):
    name_table = font["name"]
    name_table.setName(full_name, FULL_NAME, *MAC_ROMAN_IDS)  # mac
    name_table.setName(full_name, FULL_NAME, *WINDOWS_ENGLISH_IDS)  # windows
    name_table.setName(full_name_ps, POSTSCRIPT_NAME, *MAC_ROMAN_IDS)  # mac
    name_table.setName(full_name_ps, POSTSCRIPT_NAME, *WINDOWS_ENGLISH_IDS)  # windows
    name_table.setName(full_name_ps + suffix, VAR_PS_NAME_PREFIX, *MAC_ROMAN_IDS)  # mac
    name_table.setName(full_name_ps + suffix, VAR_PS_NAME_PREFIX, *WINDOWS_ENGLISH_IDS)  # windows


def fix_fullname(font, full_name, full_name_ps, suffix):
    for rec in font["name"].names:
        name_id = rec.nameID
        if name_id not in FAMILY_RELATED_IDS:
            # leave uninteresting records unmodified
            continue
        if name_id == POSTSCRIPT_NAME:
            rec.string = full_name_ps
        elif name_id == VAR_PS_NAME_PREFIX:
            rec.string = full_name_ps + suffix
        else:
            rec.string = full_name


def fix_unique_id(font, full_name_ps):
    font_id_recs = []
    new_id = ''
    name_table = font["name"]
    for rec in name_table.names:
        if rec.nameID == TRUETYPE_UNIQUE_ID:
            if new_id == '':
                old_id = rec.toUnicode()
                p = old_id.split(';')
                p.pop()
                p.append(full_name_ps)
                new_id = ';'.join(p)
            font_id_recs.append(rec)
    for rec in font_id_recs:
        name_table.setName(new_id, rec.nameID, rec.platformID, rec.platEncID, rec.langID)


def fix_notice(font, prefix):
    for rec in font["name"].names:
        name_id = rec.nameID
        if name_id == COPY_RIGHT_NOTICE:
            rec.string = prefix + " " + rec.toUnicode()


def fix_subfamily_name(font):
    for rec in font["name"].names:
        name_id = rec.nameID
        if name_id in (TYPO_SUBFAMILY_NAME, SUBFAMILY_NAME):
            if rec.toUnicode() == 'Display':
                rec.string = 'Regular'
            elif rec.toUnicode() == 'Display Italic':
                rec.string = 'Italic'


def fix_with_metadata_dict(font, dict):
    for rec in font["name"].names:
        name_id = rec.nameID
        if name_id in dict:
            rec.string = dict[name_id].format(rec.toUnicode())


def is_italic(font):
    for rec in font["name"].names:
        if rec.nameID == SUBFAMILY_NAME:
            return rec.toUnicode() == 'Italic'
    return False


def main():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('--metadata', choices=MetaData.keys())
    parser.add_argument('input', metavar='<file>',
                        help='Input font file')
    options = parser.parse_args()

    font_file = options.input
    settings = (MetaData.get(options.metadata))

    font = TTFont(font_file, recalcBBoxes=False, recalcTimestamp=False)

    # Adjust all name related fields
    full_name = settings["full_name"]
    full_name_ps = ''.join(full_name.split())

    fix_subfamily_name(font)

    suffix = ''
    if is_italic(font):
        suffix = 'Italic'
    set_full_name(font, full_name, full_name_ps, suffix)

    fix_unique_id(font, full_name_ps + suffix)
    fix_fullname(font, full_name, full_name_ps, suffix)

    # Adjust all other metadata fields
    fix_with_metadata_dict(font, settings["metadata"])

    font.save(font_file)
    font.close()


if __name__ == "__main__":
    main()
