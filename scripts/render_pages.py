#!/usr/bin/env python3
"""Script to convert csv to html table
"""
from argparse import ArgumentParser
from jinja2 import Environment, FileSystemLoader, select_autoescape
import pandas as pandas


def main():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('html', metavar='<file>',
                        help='Input html file')
    parser.add_argument('csv', nargs='+', metavar='<file>',
                        help='Input csv files')
    options = parser.parse_args()

    metadata_table = [
        pandas.read_csv(c).to_html(border=0, render_links=True) for c in options.csv
    ]

    env = Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )

    template = env.get_template(options.html)

    print(template.render(
        metadata_table=metadata_table
    ))


if __name__ == "__main__":
    main()
