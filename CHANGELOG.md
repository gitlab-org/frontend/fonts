# [1.3.0](https://gitlab.com/gitlab-org/frontend/fonts/compare/v1.2.0...v1.3.0) (2023-06-16)


### Features

* Build Italic version of GitLab Sans as well ([9be6c2a](https://gitlab.com/gitlab-org/frontend/fonts/commit/9be6c2a0ad474c530bd3374a580a17270dae1656))
* Update Inter to latest version ([f6d92b1](https://gitlab.com/gitlab-org/frontend/fonts/commit/f6d92b164d2a199dd6d7c1dae177db6862f86065))

# [1.2.0](https://gitlab.com/gitlab-org/frontend/fonts/compare/v1.1.2...v1.2.0) (2023-01-25)


### Features

* Also build Italic Version of GitLab Mono ([330c527](https://gitlab.com/gitlab-org/frontend/fonts/commit/330c527cb861dc82e78e9942204e57621469633e))
* Disable ligatures in GitLab Mono ([5bb4261](https://gitlab.com/gitlab-org/frontend/fonts/commit/5bb42610905378b5734abb2eb87a1b5d9f0a2308))
* First iteration of GitLab Mono ([2b98f48](https://gitlab.com/gitlab-org/frontend/fonts/commit/2b98f48fdd8ce772ead0c95702430dab8ccec401))
* Update jetbrains-mono to latest version (v2.304) ([fd03a1b](https://gitlab.com/gitlab-org/frontend/fonts/commit/fd03a1b98c4b109381551da09ad68681d7d71248))

## [1.1.2](https://gitlab.com/gitlab-org/frontend/fonts/compare/v1.1.1...v1.1.2) (2023-01-23)


### Bug Fixes

* Remove a few chars to fix Arabic, Persian, Georgian and CJK scripts ([fa6e6f3](https://gitlab.com/gitlab-org/frontend/fonts/commit/fa6e6f3cc61967da6b75378921f1bf33a1b00d17))

## [1.1.1](https://gitlab.com/gitlab-org/frontend/fonts/compare/v1.1.0...v1.1.1) (2023-01-23)


### Bug Fixes

* Add font-weight and font-style properties to example page ([8cdd8ee](https://gitlab.com/gitlab-org/frontend/fonts/commit/8cdd8ee43a84fe45ac498d9e2356c986f0949740))

# [1.1.0](https://gitlab.com/gitlab-org/frontend/fonts/compare/v1.0.1...v1.1.0) (2023-01-10)


### Features

* Add examples with different monospace font styling ([d2e0393](https://gitlab.com/gitlab-org/frontend/fonts/commit/d2e039314e6edb33e3f11d4bd5fe3299b4fe8513))
* Add JetBrainsMono font variants ([0955c4a](https://gitlab.com/gitlab-org/frontend/fonts/commit/0955c4a35332ae96318d1d97fe386deb171da2d2))

## [1.0.1](https://gitlab.com/gitlab-org/frontend/fonts/compare/v1.0.0...v1.0.1) (2022-12-14)


### Bug Fixes

* Update Metadata of GitLab Sans ([f6369f8](https://gitlab.com/gitlab-org/frontend/fonts/commit/f6369f849411e2612f90cae1a7485c1c2ef304e3))

# 1.0.0 (2022-12-01)


### Features

* Add distribution of jetbrains-mono ([39dae8f](https://gitlab.com/gitlab-org/frontend/fonts/commit/39dae8faf2172634186f9fdb04281e5a5ad072dc))
* Add GitLab Sans (based on Inter) ([e66afbc](https://gitlab.com/gitlab-org/frontend/fonts/commit/e66afbccec2f9e9c475f90cae4e408efce1f0d13))
