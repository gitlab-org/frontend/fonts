# GitLab fonts

- The [npm package](https://www.npmjs.com/package/@gitlab/fonts) and [distributed package](https://unpkg.com/browse/@gitlab/fonts/) contain the following WOFF2 fonts licensed under the OFL-1.1 to be used within GitLab products:
  - gitlab-mono/ based on [JetBrains Mono](https://github.com/JetBrains/JetBrainsMono) with the following adjustments:
    - Disabled ligatures by default.
    - Fixed [backtick bug]( https://github.com/JetBrains/JetBrainsMono/issues/567).
  - gitlab-sans/ based on [Inter](https://github.com/rsms/inter) with the following adjustments:
    - Use the cv05 disambiguation set (different lowercase `l` by default). 
  - jetbrains-mono/ contains fonts from https://github.com/JetBrains/JetBrainsMono.
- If you need OTF/TTF formats for GitLab Sans and GitLab Mono, you can access them from https://gitlab-org.gitlab.io/frontend/fonts/.

## Requirements for building locally

- [pdm](https://pdm.fming.dev/), on Mac run `brew install pdm`
- [jq](https://stedolan.github.io/jq/), on Mac run `brew install jq`
- make (should be installed)

In order to build the fonts, just run `make` in the root directory of the repo.

To build everything from scratch run `make clean` beforehand. 

## Support

Support will generally follow the [GitLab supported browser guidelines](https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers).
For fonts, extra stipulations need to be made for devices and operating systems.

- We will support the latest two major versions of iOS on the latest two iPhone devices, following the [browser support guidelines](https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers) for Safari only.
- We will support the latest three major versions of Android on the latest two Samsung Galaxy devices, following the [browser support guidelines](https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers) for Chrome only.
- We will support the latest two major versions of OSX, following the [browser support guidelines](https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers) for all supported browsers.
- We will support the latest two major versions of Windows, following the [browser support guidelines](https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers) for all supported browsers.
- We will support Linux distrubutions where we can, following the [browser support guidelines](https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers) for all supported browsers

Any changes made to your browser or operating system that modify the way fonts render will not be supported.

